const restify = require("restify")
const request = require("request")
const fs = require("fs")

const server = restify.createServer({
  name: "failover_gateway",
  version: "1.0.0"
});

server.use(restify.plugins.acceptParser(server.acceptable))
server.use(restify.plugins.queryParser())
server.use(restify.plugins.bodyParser())

// Information about servers
const fileContent = JSON.parse(fs.readFileSync("./auth.json"))
const { eyeglassIp, apiKeys } = fileContent

server.post("/failover", function(req, resp, next) {
  const apiKey = req.query.apiKey
  const tgtParam = req.query.failoverTarget

  if(!apiKey || !apiKeys[apiKey] || !tgtParam) {
    resp.statusCode = 401
    resp.send("unauthorized")
    return
  }
  const tgt = apiKeys[apiKey].find(x => x.failoverTarget === tgtParam)
  if(!tgt) {
    resp.statusCode = 401
    resp.send("unauthorized")
    return
  }

  const {sourceId, targetId, failoverTarget} = tgt

  if(!sourceId || !targetId || !failoverTarget) {
    resp.statusCode = 401
    resp.json({error: "unauthorized"})
    return
  }

  request.post(
    {
      url:
        "https://" +
        eyeglassIp +
        "/sera/v1/jobs?sourceid=" +
        sourceId +
        "&targetid=" +
        targetId +
        "&failovertarget=" +
        failoverTarget +
        "&controlled=true&datasync=true&configsync=true&resyncprep=true&disablemirror=false&quotasync=true",
      rejectUnauthorized: false,
      headers: {
        api_key: apiKey
      }
    },
    (error, res, body) => {
      if (error) {
        console.error(error)
        resp.send(error)
        return;
      }
      console.log(`statusCode: ${res.statusCode}`)
      console.log(body)
      resp.json(JSON.parse(body))
    }
  );

  return next()
});

server.get("/status", function(req, resp, next) {
  
  const apiKey = req.query.apiKey
  
  if(!apiKey || !apiKeys[apiKey]) {
    resp.statusCode = 401
    resp.json({error: "unauthorized"})
    return
  }
  const jobId = req.query.jobId;
  request.get(
    {
      url: "https://" + eyeglassIp + "/sera/v1/jobs/" + jobId,
      rejectUnauthorized: false,
      headers: {
        api_key: apiKey
      }
    },
    (error, res, body) => {
      if (error) {
        console.error(error)
        resp.send(error)
        return
      }
      console.log(`statusCode: ${res.statusCode}`)
      console.log(body)
      resp.json(JSON.parse(body))
    }
  )
  return next()
})

server.listen(8080, function() {
  console.log("%s listening at %s", server.ip, server.url);
})

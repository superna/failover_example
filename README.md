# Failover Gateway

The failover gateway is a small nodejs application that can sit in front of the Superna Eyeglass REST API (SERA) and provide token authentication to failover a subset of SyncIQ policies and/or Access Zones. 

# Quickstart

 - Install nodejs 8 or higher. 
 - Clone this repository: `git clone https://bitbucket.org/superna/failover_example` 
 - Install dependencies: `npm install` 
 - Configure the `auth.json` file (see below)
 - start the server: `npm start`
 
# Configuration

All configuration is done through the `auth.json` file in the root of the project. The values to be filled in can be determined by using the Superna Eyeglass REST API Explorer: 

 - Login to your eyeglass appliance.
 - From the main menu select "Eyeglass REST API"
 - Generate a REST API token, and copy to the clipboard. 
 - Select the API Explorer tab, and click the Launch API Explorer button
 - In the api_key box at the top right, paste your copied API key and click Explore
 - Use the `/nodes` route to get the IDs of the Isilon Clusters provisioned in eyeglass.
 - use the `/nodes/<id>/policies` and `/nodes/<id>/zones` routes to get the policies and access zones for a given cluster. 
 - use the information to build the `auth.json` file in the root of the failover gateway project. 
 - apply the ip address of your eyeglass appliance as the value for the `eyeglassIP` parameter in the file. 

### auth.json

The syntax of the auth.json file is as follows:
```
{
    "eyeglassIp": "172.25.1.140",
    "apiKeys": {
        "key-1": [
            {
                "targetId": "target-isilon-id",
                "sourceId": "source-isilon-id",
                "failoverTarget": "policy-name"
            },{
                "targetId": "target-isilon-id",
                "sourceId": "source-isilon-id",
                "failoverTarget": "policy-name_mirror"
            },{
                "targetId": "target-isilon-id",
                "sourceId": "source-isilon-id",
                "failoverTarget": "zone-name-1"
            }
        ],
        "key-2": [
            {
                "targetId": "target-isilon-id",
                "sourceId": "source-isilon-id",
                "failoverTarget": "policy-name-2"
            }
        ]
    }
}

```
Where in the above, the user that is provided with the `key-1` api key has authorization to failover only the policies `policy_name` and `policy_name_mirror`, as well as the access zone `zone-name-1`. The user provided with `key-2` can only failover `policy-name-2` and nothing else.  

## Example
1. Getting all isilons:  
```
GET /nodes
[
  {
    "id": "ISL-TEST-8-1-0-0-144_0050569ff3955440815c200978332081a600",
    "ip": "172.25.1.244",
    "name": "ISL-TEST-8-1-0-0-144"
  },
  {
    "id": "ISL-TEST-8-1-0-0-146_0050569f65335842815c970993ede1cfa604",
    "ip": "172.25.1.246",
    "name": "ISL-TEST-8-1-0-0-146"
  }
]
```
2. Getting all policies for each of the two isilons returned by the previous call: 
```
GET /nodes/ISL-TEST-8-1-0-0-144_0050569ff3955440815c200978332081a600/policies
[
  {
    "failoverReadiness": "ok",
    "id": "policy-ISL-TEST-8-1-0-0-144_dfs",
    "name": "dfs",
    "target": {
      "id": "ISL-TEST-8-1-0-0-146_0050569f65335842815c970993ede1cfa604",
      "ip": "172.25.1.246",
      "name": "ISL-TEST-8-1-0-0-146"
    },
    "zone": {
      "failoverReadiness": "error",
      "id": "zone-ISL-TEST-8-1-0-0-144_System",
      "name": "System"
    }
  }
]
```
```
GET /nodes/ISL-TEST-8-1-0-0-146_0050569f65335842815c970993ede1cfa604/policies
[
  {
    "failoverReadiness": "warning",
    "id": "policy-ISL-TEST-8-1-0-0-146_dfs_mirror",
    "name": "dfs_mirror",
    "target": {
      "id": "ISL-TEST-8-1-0-0-144_0050569ff3955440815c200978332081a600",
      "ip": "172.25.1.244",
      "name": "ISL-TEST-8-1-0-0-144"
    },
    "zone": {
      "failoverReadiness": "error",
      "id": "zone-ISL-TEST-8-1-0-0-146_System",
      "name": "System"
    }
  }
]
```

3. Constructing an `auth.json` file to allow failover and failback of only these policies:
```
{
    "eyeglassIp": "172.25.1.140",
    "apiKeys": {
        "igls-1uhrns90b9v4s9cggtt6vs5dlrjj38hkeapdc4ro1sdjnu0bcqtm": [
            {
                "targetId": "ISL-TEST-8-1-0-0-146_0050569f65335842815c970993ede1cfa604",
                "sourceId": "ISL-TEST-8-1-0-0-144_0050569ff3955440815c200978332081a600",
                "failoverTarget": "policy-ISL-TEST-8-1-0-0-144_dfs"
            },{
                "sourceId": "ISL-TEST-8-1-0-0-146_0050569f65335842815c970993ede1cfa604",
                "targetId": "ISL-TEST-8-1-0-0-144_0050569ff3955440815c200978332081a600",
                "failoverTarget": "policy-ISL-TEST-8-1-0-0-146_dfs_mirror"
            }
        ]
    }
}
```

 
# Usage
In order to issue calls to the server, the user has to be provided with the API token and the name of the failoverTargets they are authorized to operate on (as listed in `auth.json`).  Once this is provided, end users can use curl to launch failovers: 

```
curl -X POST http://<failover_gateway_ip>:8080/failover?apiKey=<api_key>&failoverTarget=<failoverTarget>
```

when launching failovers, the call above returns a json object with a jobId: 
```
{"id":"job-1552504373143-255009881"}
```

together with the api key, the jobId can be used to query the status of running failovers: 
```
curl http://<failover_gateway_ip>:8080/status?apiKey=<api_key>&jobId=<job_id>
```


# Securing

It is highly recommended to take the following steps to secure this deployment: 

 1. Run the software on a dual-homed system, with an interface that can reach the Isilon's management network and a second interface that is open to users.
 2. Front the software with a production-grade webserver, configured with SSL. A sample nginx configuration is provided in this repository. 
